/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Airplane;

import java.util.ArrayList;

/**
 *
 * @author hp-PC
 */
public class Airplane {
        private int ModelNum;
        private int SerialNum;
        private String airportCountry;
        private String airportName;
        private String DepAirport;
        private String ArrAirport;
        private String ArrTime;
        private int seatAvail;
        private String manufacters;
        private String ManuYear;
        private String updateTime;
        private String ExpStatus;
        private String DepTime;
       


    public String getExpStatus() {
        return ExpStatus;
    }

    public void setExpStatus(String ExpStatus) {
        this.ExpStatus = ExpStatus;
    }

   

    public double getModelNum() {
        return ModelNum;
    }

    public void setModelNum(int ModelNum) {
        this.ModelNum = ModelNum;
    }

    public double getSerialNum() {
        return SerialNum;
    }

    public void setSerialNum(int SerialNum) {
        this.SerialNum = SerialNum;
    }

    public String getAirportCountry() {
        return airportCountry;
    }

    public void setAirportCountry(String airportCountry) {
        this.airportCountry = airportCountry;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getDepAirport() {
        return DepAirport;
    }

    public void setDepAirport(String DepAirport) {
        this.DepAirport = DepAirport;
    }

    public String getArrAirport() {
        return ArrAirport;
    }

    public void setArrAirport(String ArrAirport) {
        this.ArrAirport = ArrAirport;
    }


    public String getArrTime() {
        return ArrTime;
    }

    public void setArrTime(String ArrTime) {
        this.ArrTime = ArrTime;
    }

    public int getSeatAvail() {
        return seatAvail;
    }

    public void setSeatAvail(int seatAvail) {
        this.seatAvail = seatAvail;
    }


    public String getManufacters() {
        return manufacters;
    }

    public void setManufacters(String manufacters) {
        this.manufacters = manufacters;
    }

    public String getManuYear() {
        return ManuYear;
    }

    public void setManuYear(String ManuYear) {
        this.ManuYear = ManuYear;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
    
        public String getDepTime() {
        return DepTime;
    }

    public void setDepTime(String DepTime) {
        this.DepTime = DepTime;
    }

  
    
    @Override
    public String toString()
    {
            return DepTime;
    }
        
        
    
}
