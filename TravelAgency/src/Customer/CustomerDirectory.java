/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Customer;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hp-PC
 */
public class CustomerDirectory {
  private ArrayList<Customer> cd;

    public ArrayList<Customer> getCd() {
        return cd;
    }

    public void setCd(ArrayList<Customer> cd) {
        this.cd = cd;
    }
    public CustomerDirectory(){
        this.cd = new ArrayList<>();
    }
    public Customer addCustomer(){
         Customer customer = new Customer();
         cd.add(customer);
         return customer;
    }
    public CustomerDirectory read_csv(){
          String csvFile = "Customer.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        CustomerDirectory cd = new CustomerDirectory();
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] CostomerAttribute = line.split(cvsSplitBy);
                Customer customer = cd.addCustomer();
                String Seat = CostomerAttribute[0];
                String Name = CostomerAttribute[1];
               
                customer.setSeatN(Seat);
                customer.setName(Name);
            }
            int i=1;
            for (Customer c : cd.getCd()) {
                System.out.println(i+":Seat-->"+c.getSeatN()+"\tName-->"+c.getName());
                i++;
                

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return cd;
    }
           
}
