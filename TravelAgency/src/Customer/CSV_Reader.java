/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Customer;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author hp-PC
 */
public class CSV_Reader {
   public static void main(String[] args) {
        // TODO code application logic here
          String csvFile = "Customer.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        CustomerDirectory cd = new CustomerDirectory();
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] CostomerAttribute = line.split(cvsSplitBy);
                Customer customer = cd.addCustomer();
                String Seat = CostomerAttribute[0];
                String Name = CostomerAttribute[1];
               
                customer.setSeatN(Seat);
                customer.setName(Name);
            }
            int i=1;
            for (Customer c : cd.getCd()) {
                System.out.println(i+":Seat-->"+c.getSeatN()+"\tModel-->"+c.getName());
                i++;
                

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
    }
}
