/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Schedule;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hp-PC
 */
public class Master {
        private ArrayList<FlightSchedule> ma;
        private ArrayList<Flight> ma1;

    public ArrayList<Flight> getMa1() {
        return ma1;
    }

    public void setMa1(ArrayList<Flight> ma1) {
        this.ma1 = ma1;
    }
     //   private FlightSchedule fs;
       //  FlightSchedule FlightS =new FlightSchedule();
         //Flight flight = new Flight();
        

    public ArrayList<FlightSchedule> getMa() {
        return ma;
        
    
    }

    public void setMa(ArrayList<FlightSchedule> ma) {
        this.ma = ma;
       
    }
    public Master(){
        this.ma=new ArrayList<>();
        this.ma1=new ArrayList<>();
       //FlightS=FlightS.read_csv1();
        
      //  FlightSchedule fs= new FlightSchedule();
      //  fs.read_csv1();
    }
      public Flight addFlight(){
        Flight flight = new Flight();
        ma1.add(flight);
        return flight;
    }
    public FlightSchedule addFlightSchedule(){
        FlightSchedule flightS = new FlightSchedule();
        ma.add(flightS);
        
        return flightS;
    }
    public Master read_csv(){
         String csvFile = "FlightSchedule.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        Master ma = new Master();
      
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] FlightSAttribute = line.split(cvsSplitBy);
                FlightSchedule FlightS = ma.addFlightSchedule();
                String FlightN= FlightSAttribute[0];
                String DepAirport=FlightSAttribute[1];
                String ArrAirport=FlightSAttribute[2];
                String DepTime=FlightSAttribute[3];
                String ArrTime=FlightSAttribute[4];
                
                FlightS.setFlightN(FlightN);
               FlightS.setDepAirport(DepAirport);
                FlightS.setArrAirport(ArrAirport);
                FlightS.setDepTime(DepTime);
               FlightS.setArrTime(ArrTime);
            }
            int i=1;
            for (FlightSchedule fs : ma.getMa()) {
                System.out.println(i+":FlightNumber-->"+fs.getFlightN()+"\tDepAirport-->"+fs.getDepAirport()+"\tArrAirport"+fs.getArrAirport()
                +"\tDepTime"+fs.getDepTime()+"\tArrTime"+fs.getArrTime());
                i++;
                

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ma;
    }
    
    public Master read_csv1(){
         String csvFile = "Flight.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        Master ma1 = new Master();
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] FlightAttribute = line.split(cvsSplitBy);
                Flight flight = ma1.addFlight();
                String Seat= FlightAttribute[0];
                int Price1= Integer.parseInt(FlightAttribute[1]);
                int Price2= Integer.parseInt(FlightAttribute[2]);
                int Price3= Integer.parseInt(FlightAttribute[3]);
                int Price4= Integer.parseInt(FlightAttribute[4]);
                int Price5= Integer.parseInt(FlightAttribute[5]);
                int Price6= Integer.parseInt(FlightAttribute[6]);
                int Price7= Integer.parseInt(FlightAttribute[7]);
                int Price8= Integer.parseInt(FlightAttribute[8]);
                int Price9= Integer.parseInt(FlightAttribute[9]);
                int Price10= Integer.parseInt(FlightAttribute[10]);
                int Price11= Integer.parseInt(FlightAttribute[11]);
                int Price12= Integer.parseInt(FlightAttribute[12]);
                int Price13= Integer.parseInt(FlightAttribute[13]);
                int Price14= Integer.parseInt(FlightAttribute[14]);
                
                flight.setSeatN(Seat);
                flight.setPrice1(Price1);
                flight.setPrice2(Price2);
                flight.setPrice3(Price3);
                flight.setPrice4(Price4);
                flight.setPrice5(Price5);
                flight.setPrice6(Price6);
                flight.setPrice7(Price7);
                flight.setPrice8(Price8);
                flight.setPrice9(Price9);
                flight.setPrice10(Price10);
                flight.setPrice11(Price11);
                flight.setPrice12(Price12);
                flight.setPrice13(Price13);
                flight.setPrice14(Price14);
            }
            int i=1;
            for (Flight flight : ma1.getMa1()) {
                System.out.println(i+":Seat-->"+flight.getSeatN()+"\tPrice-->"+flight.getPrice1()+"\tPrice2-->"+flight.getPrice2()
                +"\tPrice3-->"+flight.getPrice3()
                +"\tPrice4-->"+flight.getPrice4()
                +"\tPrice5-->"+flight.getPrice5()+"\tPrice6-->"+flight.getPrice6()+"\tPrice7-->"+flight.getPrice7()
                        +"\tPrice8-->"+flight.getPrice8()
                        +"\tPrice9-->"+flight.getPrice9()
                        +"\tPrice10-->"+flight.getPrice10()
                        +"\tPrice11-->"+flight.getPrice11()
                        +"\tPrice12-->"+flight.getPrice12()
                        
                );
                i++;
                

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return ma1;
    }
    
}
