/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Schedule;



/**
 *
 * @author hp-PC
 */
public class FlightSchedule {
    private String FlightN;
    private String DepAirport;
    private String ArrAirport;
    private String DepTime;
    private String ArrTime;
   
    
   
    
      

  

    public String getFlightN() {
        return FlightN;
    }

    public void setFlightN(String FlightN) {
        this.FlightN = FlightN;
    }

    public String getDepAirport() {
        return DepAirport;
    }

    public void setDepAirport(String DepAirport) {
        this.DepAirport = DepAirport;
    }

    public String getArrAirport() {
        return ArrAirport;
    }

    public void setArrAirport(String ArrAirport) {
        this.ArrAirport = ArrAirport;
    }

    public String getDepTime() {
        return DepTime;
    }

    public void setDepTime(String DepTime) {
        this.DepTime = DepTime;
    }

    public String getArrTime() {
        return ArrTime;
    }

    public void setArrTime(String ArrTime) {
        this.ArrTime = ArrTime;
    }
          
}
