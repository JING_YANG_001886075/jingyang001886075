/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Schedule;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author hp-PC
 */
public class CSV_Reader {
      public static void main(String[] args) {
        // TODO code application logic here
          String csvFile = "Flight.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        Master ma = new Master();
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] FlightAttribute = line.split(cvsSplitBy);
                Flight flight = ma.addFlight();
                String Seat = FlightAttribute[0];
                int price = Integer.parseInt(FlightAttribute[1]);
               
                flight.setSeatN(Seat);
                flight.setPrice(price);
            }
            int i=1;
            for (Flight f : ma.getMa()) {
                System.out.println(i+":Seat-->"+f.getSeatN()+"\tModel-->"+f.getPrice());
                i++;
                

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
    }
}   
