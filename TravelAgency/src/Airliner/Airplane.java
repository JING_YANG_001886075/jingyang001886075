/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Airliner;

/**
 *
 * @author hp-PC
 */
public class Airplane {
     private String FlightN;
    private int SerialN;
    private int ModelN;
    private String Manu;
    private int Year;

    public int getYear() {
        return Year;
    }

    public void setYear(int Year) {
        this.Year = Year;
    }

    public String getFlightN() {
        return FlightN;
    }

    public void setFlightN(String FlightN) {
        this.FlightN = FlightN;
    }

    public int getSerialN() {
        return SerialN;
    }

    public void setSerialN(int SerialN) {
        this.SerialN = SerialN;
    }

    public int getModelN() {
        return ModelN;
    }

    public void setModelN(int ModelN) {
        this.ModelN = ModelN;
    }

    public String getManu() {
        return Manu;
    }

    public void setManu(String Manu) {
        this.Manu = Manu;
    }
    
}
