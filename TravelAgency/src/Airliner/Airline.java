/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Airliner;

import Schedule.FlightSchedule;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hp-PC
 */
public class Airline {
        private ArrayList<Airplane> ar;
        private ArrayList<FlightSchedule> fs;

    public ArrayList<FlightSchedule> getFs() {
        return fs;
    }

    public void setFs(ArrayList<FlightSchedule> fs) {
        this.fs = fs;
    }

    public ArrayList<Airplane> getAr() {
        return ar;
    }

    public void setAr(ArrayList<Airplane> ar) {
        this.ar = ar;
    }
    public Airline(){
      this.ar= new ArrayList<>();
      this.fs= new ArrayList<>();
    }
    public Airplane addAirplane(){
      Airplane airplane = new Airplane();
      ar.add(airplane);
      return airplane;
    }
      public FlightSchedule addFlightSchedule(){
        FlightSchedule flightS = new FlightSchedule();
        fs.add(flightS);
        return flightS;
    }
    public Airline read_csv(){
        
        
         String csvFile = "Airplane.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
        Airline ar = new Airline();
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] airplaneElement = line.split(cvsSplitBy);
                Airplane airplane = ar.addAirplane();
                String FlightN = airplaneElement[0];
                int SerialN = Integer.parseInt(airplaneElement[1]);
                int ModelN = Integer.parseInt(airplaneElement[2]);
                String Manu = airplaneElement[3];
                int year = Integer.parseInt(airplaneElement[4]);
                airplane.setFlightN(FlightN);
                airplane.setSerialN(SerialN);
                airplane.setModelN(ModelN);
                airplane.setManu(Manu);
                airplane.setYear(year);
            }
            int i=1;
            for (Airplane a : ar.getAr()) {
                System.out.println(i+":FlightN-->"+a.getFlightN()+"\tModel-->"+a.getModelN()+"\tSerialNumber-->"+a.getSerialN()+"\tManufacturer-->"+a.getManu()+"\tYear-->"+a.getYear());
                i++;
                

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    return ar;
    }
    
    
}
