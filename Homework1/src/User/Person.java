/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package User;

/**
 *
 * @author hp-PC
 */
public class Person {
    private String FirstName;
    private String LastName;
    private String SSN;
    private String Gender;
    private String DateOfBirth;
    
    public Address address;
    public CreditCard creditcard;
    public FinancialAccounts financialaccounts;
    public LicenseData licensedata;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public CreditCard getCreditcard() {
        return creditcard;
    }

    public void setCreditcard(CreditCard creditcard) {
        this.creditcard = creditcard;
    }

    public FinancialAccounts getFinancialaccounts() {
        return financialaccounts;
    }

    public void setFinancialaccounts(FinancialAccounts financialaccounts) {
        this.financialaccounts = financialaccounts;
    }

    public LicenseData getLicensedata() {
        return licensedata;
    }

    public void setLicensedata(LicenseData licensedata) {
        this.licensedata = licensedata;
    }
    

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getSSN() {
        return SSN;
    }

    public void setSSN(String SSN) {
        this.SSN = SSN;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

   
    
}
