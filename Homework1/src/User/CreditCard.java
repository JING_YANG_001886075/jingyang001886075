/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package User;

/**
 *
 * @author hp-PC
 */
public class CreditCard {
    private String CardID;
    private String Holder;
    private String ExpYear;
    private String ExpMonth;
    private String Signature;

    public String getCardID() {
        return CardID;
    }

    public void setCardID(String CardID) {
        this.CardID = CardID;
    }

    public String getHolder() {
        return Holder;
    }

    public void setHolder(String Holder) {
        this.Holder = Holder;
    }

    public String getExpYear() {
        return ExpYear;
    }

    public void setExpYear(String ExpYear) {
        this.ExpYear = ExpYear;
    }

    public String getExpMonth() {
        return ExpMonth;
    }

    public void setExpMonth(String ExpMonth) {
        this.ExpMonth = ExpMonth;
    }

    public String getSignature() {
        return Signature;
    }

    public void setSignature(String Signature) {
        this.Signature = Signature;
    }

}
