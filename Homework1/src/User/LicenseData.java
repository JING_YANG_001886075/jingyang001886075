/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package User;

/**
 *
 * @author hp-PC
 */
public class LicenseData {
   private String LicenseID;
   private String IssueDate;
   private String ExpYear;
   private String ExpMonth;
   private String IssueAuthority;

    public String getLicenseID() {
        return LicenseID;
    }

    public void setLicenseID(String LicenseID) {
        this.LicenseID = LicenseID;
    }

    public String getIssueDate() {
        return IssueDate;
    }

    public void setIssueDate(String IssueDate) {
        this.IssueDate = IssueDate;
    }

    public String getExpYear() {
        return ExpYear;
    }

    public void setExpYear(String ExpYear) {
        this.ExpYear = ExpYear;
    }

    public String getExpMonth() {
        return ExpMonth;
    }

    public void setExpMonth(String ExpMonth) {
        this.ExpMonth = ExpMonth;
    }

    public String getIssueAuthority() {
        return IssueAuthority;
    }

    public void setIssueAuthority(String IssueAuthority) {
        this.IssueAuthority = IssueAuthority;
    }

    
}
