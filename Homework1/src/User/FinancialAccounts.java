/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package User;

/**
 *
 * @author hp-PC
 */
public class FinancialAccounts {
    private String CheckingNum;
    private String CheckingCreateDate;
    private String CheckingStatus;
    private String CheckingType;
    private String SavingNum;
    private String SavingCreateDate;
    private String SavingStatus;
    private String SavingType;

    public String getCheckingNum() {
        return CheckingNum;
    }

    public void setCheckingNum(String CheckingNum) {
        this.CheckingNum = CheckingNum;
    }

    public String getCheckingCreateDate() {
        return CheckingCreateDate;
    }

    public void setCheckingCreateDate(String CheckingCreateDate) {
        this.CheckingCreateDate = CheckingCreateDate;
    }

    public String getCheckingStatus() {
        return CheckingStatus;
    }

    public void setCheckingStatus(String CheckingStatus) {
        this.CheckingStatus = CheckingStatus;
    }

    public String getCheckingType() {
        return CheckingType;
    }

    public void setCheckingType(String CheckingType) {
        this.CheckingType = CheckingType;
    }

    public String getSavingNum() {
        return SavingNum;
    }

    public void setSavingNum(String SavingNum) {
        this.SavingNum = SavingNum;
    }

    public String getSavingCreateDate() {
        return SavingCreateDate;
    }

    public void setSavingCreateDate(String SavingCreateDate) {
        this.SavingCreateDate = SavingCreateDate;
    }

    public String getSavingStatus() {
        return SavingStatus;
    }

    public void setSavingStatus(String SavingStatus) {
        this.SavingStatus = SavingStatus;
    }

    public String getSavingType() {
        return SavingType;
    }

    public void setSavingType(String SavingType) {
        this.SavingType = SavingType;
    }
    
}
