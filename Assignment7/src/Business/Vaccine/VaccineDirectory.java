/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Vaccine;


import java.util.ArrayList;

/**
 *
 * @author yangjing
 */
public class VaccineDirectory {
    private  ArrayList<Vaccine> vaccines;
    

    public VaccineDirectory() {
        vaccines = new ArrayList<>();
    }

    public ArrayList<Vaccine> getVaccines() {
        return vaccines;
    }
    
    public Vaccine createVaccine(String name){
       Vaccine vaccine = new Vaccine();
       vaccines.add(vaccine);
       return vaccine;
    }
    
   
       
    
}
