/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Person.PersonDirectory;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.Vaccine.VaccineDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author yangjing
 */
public abstract class Organization {
    private String name;
    private WorkQueue workqueue;
    private PersonDirectory personDirectory;
    private UserAccountDirectory userAccountDirectory;
    private VaccineDirectory vaccineDirectory;
    private int organizationID;
    private static int counter;
    
    
    public enum Type{
        Admin("AdminOrganization"){},
        Inventory("InventoryOrganization"){},
        Clinic("ClinicOrganization"){},
        Supplier("SupplierOrganization"){},
        Distributor("DistributorOrganization"){},
        Provider("ProviderOrganization"){};
        
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        workqueue = new WorkQueue();
        personDirectory = new PersonDirectory();
        userAccountDirectory = new UserAccountDirectory();
        vaccineDirectory = new VaccineDirectory();
        organizationID = counter;
        ++counter;
    }

    public abstract ArrayList<Role> getSupportedRole();

    public VaccineDirectory getVaccineDirectory() {
        return vaccineDirectory;
    }
    
    
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public WorkQueue getWorkQueue() {
        return workqueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workqueue = workQueue;
    }
    
    public int getOrganizationID() {
        return organizationID;
    }

    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }  
    
    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
    
}
