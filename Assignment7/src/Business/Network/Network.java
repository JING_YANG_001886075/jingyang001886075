/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import Business.Enterprise.EnterpriseDirectory;
import Business.Organization.Organization;
import Business.State.StateDirectory;
import java.util.ArrayList;

/**
 *
 * @author yangjing
 */
public class Network {
    private String name; 
    private EnterpriseDirectory enterpriseDirectory;
    private StateDirectory stateDirectory;

    public Network() {
        stateDirectory = new StateDirectory();
        enterpriseDirectory = new EnterpriseDirectory();
    }

    public StateDirectory getStateDirectory() {
        return stateDirectory;
    }

    public void setStateDirectory(StateDirectory stateDirectory) {
        this.stateDirectory = stateDirectory;
    }
    

    
    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    
}
