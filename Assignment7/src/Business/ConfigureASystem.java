/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Person.Person;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;
import Business.Vaccine.Vaccine;
import Business.Vaccine.VaccineDirectory;

/**
 *
 * @author yangjing
 */
public class ConfigureASystem {
    public static EcoSystem configure(){
        
        EcoSystem system = EcoSystem.getInstance();
        
        //Create a network
        //create an enterprise
        //initialize some organizations
        //have some employees 
        //create user account
        
        
        Person person = system.getPersonDirectory().createPerson("Jing");
        UserAccount ua = system.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", person, new SystemAdminRole());
        
             
//        VaccineDirectory vaccineInventory1 = new VaccineDirectory();
//        VaccineDirectory vaccineInventory2 = new VaccineDirectory();
//        VaccineDirectory vaccineInventory3 = new VaccineDirectory();
//        VaccineDirectory vaccineInventory4 = new VaccineDirectory();
//        Vaccine vaccine1 = new Vaccine();
//        Vaccine vaccine2 = new Vaccine();
//        Vaccine vaccine3 = new Vaccine();
//        Vaccine vaccine4 = new Vaccine();
//        vaccine1.setName("Live vaccines");
//        vaccine2.setName("Subunit vaccines");
//        vaccine3.setName("Toxoid vaccines");
//        vaccine4.setName("DNA vaccines");
//        vaccineInventory1.getVaccines().add(vaccine1);
//        vaccineInventory1.setTotal(100);
//        vaccineInventory2.getVaccines().add(vaccine2);
//        vaccineInventory2.setTotal(100);
//        vaccineInventory3.getVaccines().add(vaccine3);
//        vaccineInventory3.setTotal(100);
//        vaccineInventory4.getVaccines().add(vaccine4);
//        vaccineInventory4.setTotal(100);
        
        return system;
    }
    
}
