/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.Organization;
import Business.Organization.ProviderOrganization;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author yangjing
 */
public class ProviderEnterprise extends Enterprise{
    
    public ProviderEnterprise(String name) {
        super(name,EnterpriseType.Provider);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }
    
    public ArrayList<Organization> getSupportedOrganization() {
        ArrayList<Organization> organizations = new ArrayList<>();
        organizations.add(new ProviderOrganization());
        return organizations;
    }
    
}
