/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.Organization;
import Business.Organization.SupplierOrganization;
import Business.Role.Role;

import Business.Vaccine.VaccineDirectory;
import java.util.ArrayList;

/**
 *
 * @author yangjing
 */
public class Supplier extends Enterprise{
    private VaccineDirectory vaccineDirectory;

    public VaccineDirectory getVaccineDirectory() {
        return vaccineDirectory;
    }

    public Supplier(String name) {
        super(name,EnterpriseType.Supplier);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }
    
    public ArrayList<Organization> getSupportedOrganization() {
        ArrayList<Organization> organizations = new ArrayList<>();
        organizations.add(new SupplierOrganization());
        return organizations;
    }
    
}
