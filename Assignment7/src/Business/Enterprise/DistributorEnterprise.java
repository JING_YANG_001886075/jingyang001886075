/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.DistributorOrganization;
import Business.Organization.ManageInventoryOrganization;
import Business.Organization.Organization;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author yangjing
 */
public class DistributorEnterprise extends Enterprise{
    
    public DistributorEnterprise(String name) {
        super(name,EnterpriseType.Distributor);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }
    
    public ArrayList<Organization> getSupportedOrganization() {
        ArrayList<Organization> organizations = new ArrayList<>();
        organizations.add(new ManageInventoryOrganization());
        organizations.add(new DistributorOrganization());
        return organizations;
    }
    
}
