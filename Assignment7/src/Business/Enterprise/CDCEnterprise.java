/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Disease.DiseaseDirectory;
import Business.Organization.ClinicOrganization;
import Business.Organization.Organization;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author yangjing
 */
public class CDCEnterprise extends Enterprise{
    private DiseaseDirectory diseaseDirectory;
    
    public CDCEnterprise(String name) {
        super(name,EnterpriseType.CDC);
        diseaseDirectory = new DiseaseDirectory();
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }

    @Override
    public ArrayList<Organization> getSupportedOrganization() {
        ArrayList<Organization> organizations = new ArrayList<>();
        organizations.add(new ClinicOrganization());
        return organizations;
    }
    
}
