/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.HumanResources.PersonDirectory;

import Bussiness.HumanResources.Person.Person;
import java.util.ArrayList;

/**
 *
 * @author yangjing
 */
public class PersonDirectory {
    private ArrayList<Person> personlist;

    public PersonDirectory() {
        personlist= new ArrayList();
    }

    public ArrayList<Person> getPersonlist() {
        return personlist;
    }

    public void setPersonlist(ArrayList<Person> personlist) {
        this.personlist = personlist;
    }
    
    public Person newPerson(){
        Person p = new Person();
        personlist.add(p);
        return p;
    }
    
    public Person findPersonByLastName(String lastName){
        for(Person p:personlist){
            if(p.getLastName().equals(lastName)){
                return p;
            }           
        }
        return null;
    }
    
     public boolean findPersonByLastN(String lastName){
        for(Person p:personlist){
            if(p.getLastName().equals(lastName)){
                return true;
            }           
        }
        return false;
    }
    
    
    public Person findPersonByID(String UserId){
        for(Person p:personlist){
            if(p.getLastName().equals(UserId)){
                return p;
            }           
        }
        return null;
    }
    
    
}
