/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.SystemAdministration;

import Bussiness.HumanResources.Person.Person;
import java.util.ArrayList;

/**
 *
 * @author yangjing
 */
public class UserAccountDirectory {
    private ArrayList<UserAccount> UserAccountDirectory;
    private Person person;

    public UserAccountDirectory() {
        UserAccountDirectory=new ArrayList();
    }

   

    
    public ArrayList<UserAccount> getUserAccountDirectory() {
        return UserAccountDirectory;
    }

    public void setUserAccountDirectory(ArrayList<UserAccount> UserAccountDirectory) {
        this.UserAccountDirectory = UserAccountDirectory;
    }
    
    public UserAccount newUserAccount(){
        UserAccount ua = new UserAccount();
        UserAccountDirectory.add(ua);
        return ua;
    }
    public UserAccount isValidUser(String userid,String pwd){
        for (UserAccount ua:UserAccountDirectory){
            if((ua.getUserID().equals(userid))&&(ua.getPassword().equals(pwd))){
                return ua;
            }
        }   
        return null;
            }
    
    public boolean isValid(String userid,String pwd){
        for (UserAccount ua:UserAccountDirectory){
            if((ua.getUserID().equals(userid))&&(ua.getPassword().equals(pwd))){
                return true;
            }
        }   
        return false;
            }
    
    public UserAccount findUserAccount(String userid){
        for (UserAccount ua:UserAccountDirectory){
               if(ua.getUserID().equals(userid)){
                return ua;
             
            }
                     
       }
    
    return null;
    }
    
    public boolean findUserA(String userid){
        for (UserAccount ua:UserAccountDirectory){
            if(ua.getUserID().equals(userid)){
                return true;
            }          
       }
    
    return false;
    }
    
}
