/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness;

import Bussiness.HumanResources.PersonDirectory.PersonDirectory;
import Bussiness.SystemAdministration.UserAccountDirectory;

/**
 *
 * @author yangjing
 */
public class Business {
    private String name;
    private PersonDirectory personDirectory;
    private UserAccountDirectory userAccountDirectory;

    public Business(String n) {
        name=n;
        personDirectory= new PersonDirectory();
        userAccountDirectory= new UserAccountDirectory();
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }
    
    
    
}
