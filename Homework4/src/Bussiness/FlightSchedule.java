/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness;

import java.util.ArrayList;

/**
 *
 * @author yangjing
 */
public class FlightSchedule {
    private ArrayList<Flight> flightSchedule;
    
     public FlightSchedule() {
        this.flightSchedule = new ArrayList<>();    
    }

    public ArrayList<Flight> getFlightSchedule() {
        return flightSchedule;
    }

    public void setFlightSchedule(ArrayList<Flight> flightSchedule) {
        this.flightSchedule = flightSchedule;
    }
    public Flight addFlight(){
        Flight flight = new Flight();
        flightSchedule.add(flight);
        return flight;
    }
    
   
    
    public void deleteFlight(Flight flight){
        flightSchedule.remove(flight);
    }
    
    public Flight searchFlight(String flightNum){
        for (Flight account : flightSchedule){
            if (account.getFlightNum().equals(flightNum)){
                return account;
            } 
        }
        return null;
     }
    
   
    
    
    public Flight searchFlight1(String depature,String arrive){
        for (Flight account : flightSchedule){
            if ((account.getDepAirport().equals(depature))&&(account.getArrAirport().equals(arrive))){
                return account;
            } 
        }
        return null;
     }
    
    
}
