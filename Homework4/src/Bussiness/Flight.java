/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness;

/**
 *
 * @author yangjing
 */
public class Flight {
    private String FlightNum;
    private String DepAirport;
    private String ArrAirport;
    private String DepTime;
    private String ArrTime;
    private int SeatAva;
    private int price;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getSeatAva() {
        return SeatAva;
    }

    public void setSeatAva(int SeatAva) {
        this.SeatAva = SeatAva;
    }
            

    public String getFlightNum() {
        return FlightNum;
    }

    public void setFlightNum(String FlightNum) {
        this.FlightNum = FlightNum;
    }

    public String getDepAirport() {
        return DepAirport;
    }

    public void setDepAirport(String DepAirport) {
        this.DepAirport = DepAirport;
    }

    public String getArrAirport() {
        return ArrAirport;
    }

    public void setArrAirport(String ArrAirport) {
        this.ArrAirport = ArrAirport;
    }

    public String getDepTime() {
        return DepTime;
    }

    public void setDepTime(String DepTime) {
        this.DepTime = DepTime;
    }

    public String getArrTime() {
        return ArrTime;
    }

    public void setArrTime(String ArrTime) {
        this.ArrTime = ArrTime;
    }
    
    public String toString(){
        return FlightNum;
    }
    
}
